import { memo } from 'react';
import { Handle, Position } from 'reactflow';
import './styles.scss';

const handleLabels = {
  taskStatus: 'Status',
  duration: 'Duration',
};

const SimpleInput = ({ data, isConnectable }) => {
  const renderHandle = (id) => (
    <Handle
      type="source"
      position={Position.Bottom}
      id={id}
      className={`handle-${id}`}
      isConnectable={isConnectable}
      onConnect={data.simpleInputsOnConnectHandle}
    >
      <div className="handleLabel">{handleLabels[id]}</div>
    </Handle>
  );

  return (
    <div className="simple-input">
      <div>Input Node:</div>
      <div className="datasource">
        DataSource Shape:
        <pre>{JSON.stringify(data.parameters, null, 2)}</pre>
      </div>
      {renderHandle('taskStatus')}
      {renderHandle('duration')}
    </div>
  );
};

export default memo(SimpleInput);

import JSONPretty from 'react-json-pretty';
import 'react-json-pretty/themes/monikai.css';
import './styles.scss';

const CodeViewer = ({ nodesJsonCode }) => {
  if (!nodesJsonCode || typeof nodesJsonCode !== 'string') {
    return null;
  }
  return (
    <div className="code-viewer">
      <JSONPretty id="json-pretty" data={nodesJsonCode}></JSONPretty>
    </div>
  );
};

export default CodeViewer;

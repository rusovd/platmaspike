export const initData = {
  nodes: [
    {
      id: '1',
      type: 'simpleInput',
      data: {
        parameters: {
          taskStatus: {
            type: 'string',
            label: 'Status',
          },
          duration: {
            type: 'number',
            label: 'Duration',
          },
        },
      },
      position: {
        x: 200.5,
        y: -103,
      },
      width: 89,
      height: 113,
      selected: false,
      positionAbsolute: {
        x: 200.5,
        y: -103,
      },
      dragging: false,
    },
  ],
  edges: [],
};

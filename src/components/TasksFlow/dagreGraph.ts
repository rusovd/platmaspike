import dagre from 'dagre';

const dagreGraph = new dagre.graphlib.Graph();
dagreGraph.setDefaultEdgeLabel(() => ({}));

const nodeWidth = 100;
const nodeHeight = 80;

export const getLayoutedElements = (
  nodes: any[],
  edges: any[],
  direction = 'TB'
) => {
  const isHorizontal = direction === 'LR';
  dagreGraph.setGraph({ rankdir: direction });

  nodes?.forEach((node: { id: string }) => {
    dagreGraph.setNode(node.id, { width: nodeWidth, height: nodeHeight });
  });

  edges?.forEach(
    (edge: {
      source: dagre.Edge;
      target: string | { [key: string]: any } | undefined;
    }) => {
      dagreGraph.setEdge(edge.source, edge.target);
    }
  );

  dagre.layout(dagreGraph);

  nodes?.forEach(
    (node: {
      id: string | dagre.Label;
      targetPosition: string;
      sourcePosition: string;
      position: { x: number; y: number };
    }) => {
      const nodeWithPosition = dagreGraph.node(node.id);
      node.targetPosition = isHorizontal ? 'left' : 'top';
      node.sourcePosition = isHorizontal ? 'right' : 'bottom';

      node.position = {
        x: nodeWithPosition.x - nodeWidth / 2,
        y: nodeWithPosition.y - nodeHeight / 2,
      };

      return node;
    }
  );

  return { nodes, edges };
};

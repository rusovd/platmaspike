import { useState, useRef, useCallback, useEffect } from 'react';
import ReactFlow, {
  addEdge,
  Controls,
  applyNodeChanges,
  applyEdgeChanges,
} from 'reactflow';
import 'reactflow/dist/style.css';
import useUndoable from 'use-undoable';
import { SideBar } from '../SideBar';
import { ConditionsNode } from '../ConditionsNode';
import { SimpleInput } from '../SimpleInput';
import { initData } from './initData';
import { getLayoutedElements } from './dagreGraph';
import './styles.scss';

const nodeTypes = {
  simpleInput: SimpleInput,
  conditionsNode: ConditionsNode,
};

let id = 0;
const getId = () => `task-node_${id++}`;

const useConnectionUpdate = (elements, setElements) => {
  return useCallback(
    (connection) => {
      const updatedNodes = elements.nodes.map((node) => {
        if (node.id === connection.target) {
          const sourceNode = elements.nodes.find(
            (n) => n.id === connection.source
          );
          if (sourceNode.type === 'simpleInput') {
            return {
              ...node,
              data: {
                ...node.data,
                inParam: sourceNode.data?.parameters[connection.sourceHandle],
              },
            };
          }
        }
        return node;
      });
      setElements((prev) => ({
        ...prev,
        nodes: updatedNodes,
        edges: addEdge(connection, elements.edges),
      }));
    },
    [elements, setElements]
  );
};

const TasksFlow = () => {
  const reactFlowWrapper = useRef(null);
  const [reactFlowInstance, setReactFlowInstance] = useState(null);
  const [elements, setElements, { undo, canUndo, redo, canRedo }] = useUndoable(
    { nodes: [], edges: [] },
    { behavior: 'destroyFuture' }
  );

  useEffect(() => {
    setElements((state) => ({
      ...state,
      ...initData,
    }));
  }, []);

  const handleChange = useCallback(
    (type) => (changes) => {
      const ignoredTypes =
        type === 'nodes' ? ['select', 'position', 'dimensions'] : ['select'];
      const ignore = ignoredTypes.includes(changes[0].type);
      const updateFunc = type === 'nodes' ? applyNodeChanges : applyEdgeChanges;

      setElements(
        (prev) => ({
          nodes:
            type === 'nodes' ? updateFunc(changes, elements[type]) : prev.nodes,
          edges:
            type === 'edges' ? updateFunc(changes, elements[type]) : prev.edges,
        }),
        'destroyFuture',
        ignore
      );
    },
    [setElements, elements]
  );

  const handleDragOver = useCallback((e) => {
    e.preventDefault();
  }, []);

  const onConnect = useConnectionUpdate(elements, setElements);

  const onLayout = useCallback(
    (direction) => {
      const { nodes, edges } = getLayoutedElements(
        elements.nodes,
        elements.edges,
        direction
      );
    },
    [elements]
  );

  const onDrop = useCallback(
    (event) => {
      event.preventDefault();
      const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
      const type = event.dataTransfer.getData('application/reactflow');
      if (!type) return;
      const position = reactFlowInstance.project({
        x: event.clientX - reactFlowBounds.left,
        y: event.clientY - reactFlowBounds.top,
      });
      const newNode = {
        id: getId(),
        type,
        position,
        data: { label: `${type} node` },
      };
      setElements((prev) => ({
        ...prev,
        nodes: [...prev.nodes, newNode],
      }));
    },
    [reactFlowInstance, setElements]
  );

  return (
    <div className="task-flow">
      <div className="reactflow-wrapper" ref={reactFlowWrapper}>
        <ReactFlow
          nodes={elements.nodes}
          nodeTypes={nodeTypes}
          edges={elements.edges}
          onNodesChange={handleChange('nodes')}
          onEdgesChange={handleChange('edges')}
          onConnect={onConnect}
          onInit={setReactFlowInstance}
          onDrop={onDrop}
          onDragOver={handleDragOver}
          fitView
        >
          <Controls style={{ display: 'flex', flexDirection: 'raw' }}>
            <button disabled={!canUndo} onClick={undo}>
              U
            </button>
            <button disabled={!canRedo} onClick={redo}>
              R
            </button>
            <button onClick={() => onLayout('TB')}>V</button>
            <button onClick={() => onLayout('LR')}>H</button>
          </Controls>
        </ReactFlow>
      </div>
      <SideBar nodesJsonCode={JSON.stringify(elements)} />
    </div>
  );
};

export default TasksFlow;

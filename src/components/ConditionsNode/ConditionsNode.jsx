import { memo } from 'react';
import { Handle, Position } from 'reactflow';
import './styles.scss';

const getOptionsByType = (type) => {
  const options = {
    string: [
      { value: 'contains', label: 'contains' },
      { value: 'in', label: 'IN' },
    ],
    number: [
      { value: 'gte', label: 'Greater Than' },
      { value: 'lte', label: 'Less Than' },
    ],
  };

  return options[type] || [];
};

const ConditionNode = memo(({ data, isConnectable }) => {
  const paramInConnectHandle = (params) => {
    console.log('paramInConnectHandle :::🚦', params);
  };

  const paramType = data.inParam?.type;

  ConditionNode.displayName = 'ConditionNode';

  return (
    <div className="condition-node">
      <Handle
        type="target"
        position={Position.Top}
        id="condition"
        className="top-handle"
        onConnect={paramInConnectHandle}
        isConnectable={isConnectable}
      />
      <div>
        Condition Node: <strong>{data.color}</strong>
      </div>
      <div className="param-info">
        {data.inParam ? (
          <div>Param: {data.inParam.label}</div>
        ) : (
          'Add an input param to this node!'
        )}
      </div>
      {data.inParam && (
        <div className="input-section">
          <select name="ops" id="ops" defaultValue="eq">
            <option value="eq">equals</option>
            <option value="neq">not equal</option>
            {getOptionsByType(paramType).map((option) => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))}
          </select>
          <input onChange={() => {}} defaultValue={''} />
        </div>
      )}
      <Handle
        type="source"
        position={Position.Bottom}
        id="a"
        className="bottom-handle left"
        isConnectable={isConnectable}
      >
        <div className="label">True</div>
      </Handle>
      <Handle
        type="source"
        position={Position.Bottom}
        id="b"
        className="bottom-handle right"
        isConnectable={isConnectable}
      >
        <div className="label">False</div>
      </Handle>
    </div>
  );
});

export default ConditionNode;

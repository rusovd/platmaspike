import { CodeViewer } from '../CodeViewer';

const nodes = [
  {
    className: 'task-node input',
    type: 'simpleInput',
    label: 'Simple Input',
  },
  {
    className: 'task-node',
    type: 'conditionsNode',
    label: 'Condition Node',
  },
  {
    className: 'task-node output',
    type: 'output',
    label: 'Output Node',
  },
];

const SideBar = ({ nodesJsonCode }) => {
  const onDragStart = (event, nodeType) => {
    event.dataTransfer.setData('application/reactflow', nodeType);
    event.dataTransfer.effectAllowed = 'move';
    console.log('onDragStart :::🚦', nodeType);
  };

  return (
    <aside>
      {nodes.map((node, index) => (
        <div
          key={index}
          className={node.className}
          onDragStart={(event) => onDragStart(event, node.type)}
          draggable
        >
          {node.label}
        </div>
      ))}
      <CodeViewer nodesJsonCode={nodesJsonCode} />
    </aside>
  );
};

SideBar.displayName = 'SideBar';

export default SideBar;

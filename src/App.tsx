import TasksFlow from './components/TasksFlow';
import { ReactFlowProvider } from 'reactflow';

export default function App() {
  return (
    <ReactFlowProvider>
      <div className="App">
        <TasksFlow />
      </div>
    </ReactFlowProvider>
  );
}
